//
//  testingApp.swift
//  testing
//
//  Created by alvin hariyono on 29/09/21.
//

import SwiftUI

@main
struct testingApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
